

<!-- include part theme -->
<?php  include 'views/includes/header.php';?>
<!-- include part theme -->
<?php  include 'views/includes/navbarfront.php';?>

<br>


<div class='container'>
    <div class="row justify-content-md-center">
        
        <div class="col-10">
            <center><img src='assets/uploads/<?php echo $row['foto']?>?>' class="img-fluid"></center>
        </div>
        
        <div class="col-10">

            <div class="row">
                <div class="col-12">
                    <p class='text-left'><?php echo $row['keterangan']?></p>
                </div>
                
            </div>

            <div class="row">
                <div class="col-2">
                    <p class='text-left'>Nama Gunung</p>
                </div>
                <div class="col-10">
                    <p class='text-left'><?php echo $row['nama_gunung']?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    <p class='text-left'>Alamat</p>
                </div>
                <div class="col-10">
                    <p class='text-left'><?php echo $row['alamat']?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    <p class='text-left'>Harga</p>
                </div>
                <div class="col-10">
                    <p class='text-left'><?php echo $row['harga']?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    <p class='text-left'>Fasilitas</p>
                </div>
                <div class="col-10">
                    <p class='text-left'><?php echo $row['fasilitas']?>/100</p>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    <p class='text-left'>Jam Operasional</p>
                </div>
                <div class="col-10">
                    <p class='text-left'><?php echo $row['jamoperasional']?> Jam</p>
                </div>
            </div>


            <div class="row">
                <div class="col-12">
                <p class='text-left'>Lokasi</p>
                </div>
                <div class="col-12">
                    <?php echo $row['maps']?>
                </div>
                
            </div>

           
            
            
            
        </div>
        
    </div>

</div>

                 


<?php  include 'views/includes/footer.php';?>
<!-- end include footer part theme -->